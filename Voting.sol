pragma solidity ^0.4.18;

contract Voting {
    // Событие для клиента, вызываемое при добавлении кандидата
    event AddedCandidate(uint candidateID);

    // Структура описывает голосующего и идентификатор кандидата, за которого он проголосовал
    struct Voter {
        bytes32 uid; // bytes32 type are basically strings
        uint candidateIDVote;
    }
    // Структура описывает кандидата
    struct Candidate {
        bytes32 name;
        bytes32 party;
        // Принимает значение true, когда кандидат существует
        bool doesExist;
    }

    // Количество кандидатов и проголосовавших
    uint numCandidates;
    uint numVoters;

    // Хэш-таблицы, позволяющие быстро найти кандидата и проголосовавшего по идентификатору
    mapping (uint => Candidate) candidates;
    mapping (uint => Voter) voters;

    // Функция добавления кандидата
    function addCandidate(bytes32 name, bytes32 party) public {
        // candidateID - возвращаемая переменная
        uint candidateID = numCandidates++;
        // Сохранение кандидата в базу данных
        candidates[candidateID] = Candidate(name,party,true);
        AddedCandidate(candidateID);
    }

    // Функция голосования
    function vote(bytes32 uid, uint candidateID) public {
        if (candidates[candidateID].doesExist == true) {
            uint voterID = numVoters++; //voterID - возвращаемая переменная
            voters[voterID] = Voter(uid,candidateID);
        }
    }

    // функция, возвращающая количество голосов за указанного кандидата (только чтение)
    function totalVotes(uint candidateID) view public returns (uint) {
        uint numOfVotes = 0; // we will return this
        for (uint i = 0; i < numVoters; i++) {
            // if the voter votes for this specific candidate, we increment the number
            if (voters[i].candidateIDVote == candidateID) {
                numOfVotes++;
            }
        }
        return numOfVotes;
    }

    // Функция, возвращающая количество кандидатов
    function getNumOfCandidates() public view returns(uint) {
        return numCandidates;
    }

    // Функция, возвращающая количество голосов (общее)
    function getNumOfVoters() public view returns(uint) {
        return numVoters;
    }

    // Функция, возвращающая информацию о кандидате
    function getCandidate(uint candidateID) public view returns (uint,bytes32, bytes32) {
        return (candidateID,candidates[candidateID].name,candidates[candidateID].party);
    }
}